import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DataService } from '../../providers/data-service';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    data: any = [];
    Name: string;
    Type: string;
    EmpName: number;
    employ: any = [];
    PostData: any;
    constructor(public DS: DataService, public navCtrl: NavController) {

    }

    getData() {
        console.log('---------Entered getData in home.ts--------');
        this.data = [];
        this.DS.getAllData().then((data) => {
            for (let i in data) {
                this.data[i] = data[i];
            }
        });
        console.log('---------Exiting getData in home.ts--------');
    }

    postData() {
        console.log('---------Entered postData in home.ts--------');
        // let empnum = parseInt(this.EmpNum);
        this.data = [];
        let NewEmp, flag = 0;
        this.DS.getAllData().then((data) => {
            for (let i in data) {
                if (data[i].Name == this.Name) {
                    NewEmp = {
                        "id": data[i].employ.length + 1,
                        "Name": this.EmpName
                    }
                    data[i].employ.push(NewEmp);
                    this.PostData = data[i];
                    flag = 1;
                    console.log(this.PostData);
                }
            }
        }).then(() => {
            if (flag == 1) {
                console.log(this.PostData);
                this.DS.postData(this.PostData);
            }
        }).then(() => {
            if (flag != 1) {
                this.PostData = {
                    "Name": this.Name,
                    "type": this.Type,
                    "employ": [{
                        "id": "01",
                        "Name": this.EmpName
                    }]
                }
                this.DS.postData(this.PostData);
            }
        })

        console.log('---------Exiting postData in home.ts--------');
    }

    removeData(Name) {
      console.log('---------Entered removeData in home.ts--------');
        let data = {
            "Name": Name
        };
        this.DS.deleteData(data);
        console.log('---------Exiting removeData in home.ts--------');
    }
}