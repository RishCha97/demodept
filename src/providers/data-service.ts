import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';

@Injectable()
export class DataService {
  db : any;
  remote : any;
  data : any;
  constructor(public http: Http) {
    this.db = new PouchDB('dept');
    this.remote = 'http://localhost:5984/dept';

    let options = {
      live: true,
      retry: true,
      continuous: true
    };
 
    this.db.sync(this.remote, options);
  }
/**
 * to get all documents
 */
  getAllData(){
    console.log('-----------Entered getAllData in DataService-----------');

    return new Promise(resolve => {
      
      this.db.allDocs({
        include_docs : true
      }).then((result) => {
        this.data = [];

        let docs = result.rows.map((row)=>{
          this.data.push(row.doc);
        });
        console.log('-----------Exiting getAllData in DataService-----------');
        resolve(this.data);

        this.db.changes({live:true , since:'now' , include_docs : true}).on('change' , (change)=> {
          this.handleChange(change);
        });
      }).catch((error)=>{
        console.log(error);
      });
    });
    
  }

/**
 * to post a new document or just update a previous one
 */
  postData(data){
  console.log('-----------Entered postData in DataService-----------');
    return new Promise(resolve => {
      
      this.db.allDocs({
        include_docs : true
      }).then((result) => {
        this.data = [];
        let flag = 0;    //flag = 0 => data not found
        result.rows.map((row)=>{
          
          //Name of Dept is matched but a new employ is there to be added
          if(data.Name == row.doc.Name){          
              row.doc.employ = data.employ;
              flag =1;
              console.log(row.doc);
              this.db.post(row.doc).catch((error) => {
                console.log(error);
              })
          }
        });
         if(flag == 0){
            console.log(data);
            this.db.post(data);
          }
        console.log('-----------Exiting postData in DataService-----------');
        resolve(this.data);

        this.db.changes({live:true , since:'now' , include_docs : true}).on('change' , (change)=> {
          this.handleChange(change);
        });
      }).catch((error)=>{
        console.log(error);
      });
      
    });
  }

  /**
   * to delete the given document if exists
   */

  deleteData(data){
console.log('-----------Entered deleteData in DataService-----------');
     return new Promise(resolve => {
      
      this.db.allDocs({
        include_docs : true
      }).then((result) => {
        this.data = [];

        let docs = result.rows.map((row)=>{
          //Name of Dept is matched 
          if(data.Name == row.doc.Name){          
              this.db.remove(row.doc).catch((error) => {
                console.log(error);
              })
          }
        });
        console.log('-----------Exiting deleteData in DataService-----------');
        resolve(this.data);

        this.db.changes({live:true , since:'now' , include_docs : true}).on('change' , (change)=> {
          this.handleChange(change);
        });
      }).catch((error)=>{
        console.log(error);
      });
    });

  }

  handleChange(change){
console.log('-----------Entered handleChange in DataService-----------');
     let changedDoc = null;
  let changedIndex = null;
 
  this.data.forEach((doc, index) => {
 
    if(doc._id === change.id){
      changedDoc = doc;
      changedIndex = index;
    }
 
  });
 
  //A document was deleted
  if(change.deleted){
    this.data.splice(changedIndex, 1);
  } 
  else {
 
    //A document was updated
    if(changedDoc){
      this.data[changedIndex] = change.doc;
    } 
 
    //A document was added
    else {
      this.data.push(change.doc); 
    }
 
  }
console.log('-----------Exiting handleCHange in DataService-----------');
  }
}
